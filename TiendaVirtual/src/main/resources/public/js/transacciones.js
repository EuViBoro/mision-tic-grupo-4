
function loadData(){
    let request = sendRequest('transaccion/list', 'GET', '')
    let table = document.getElementById('transaccion-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idTransaccion}</th>
                    <td>${element.tipoTransaccion}</td>
                    <td>${element.fechaTransaccion}</td>
                    <td>${element.vendedor}</td>
                    <td>${element.comprador}</td>
                    <td>${element.total}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_transaccion.html?id=${element.idTransaccion}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteTransaccion(${element.idTransaccion})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadTransaccion(idTransaccion){
    let request = sendRequest('transaccion/list/'+idTransaccion, 'GET', '')
    let tipo = document.getElementById('transa-tipo')
    let fecha = document.getElementById('transa-fecha')
    let seller = document.getElementById('transa-seller')
    let buyer = document.getElementById('transa-buyer')
    let tota = document.getElementById('transa-total-value')
    let id = document.getElementById('transa-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.idTransaccion
        tipo.value = data.tipoTransaccion
        fecha.value = data.fechaTransaccion
        seller.value = data.vendedor
        buyer.value = data.comprador
        tota.value = data.total
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteTransaccion(idTransaccion){
    let request = sendRequest('transaccion/'+idTransaccion, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveTransaccion(){

    let tipo = document.getElementById('transa-tipo').value
    let fecha = document.getElementById('transa-fecha').value
    let seller = document.getElementById('transa-seller').value
    let buyer = document.getElementById('transa-buyer').value
    let tota = document.getElementById('transa-total-value').value
    let id = document.getElementById('transa-id').value
    let data = {'idTransaccion': id,'tipoTransaccion':tipo,'fechaTransaccion': fecha, 'vendedor': seller, 'comprador': buyer, 'total':tota }
    let request = sendRequest('transaccion/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'transaccion.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}