function loadData(){
  let request = sendRequest('usuario/list', 'GET', '')
  let table = document.getElementById('usuario-table');
  table.innerHTML = "";
  request.onload = function(){
      
      let data = request.response;
      console.log(data);
      data.forEach((element, index) => {
          table.innerHTML += `
              <tr>
                  <th>${element.idUsuario}</th>
                  <td>${element.nombreUsuario}</td>
                  <td>${element.contrasenaUsuario}</td>
              </tr>

              `
      });
  }
  request.onerror = function(){
      table.innerHTML = `
          <tr>
              <td colspan="6">Error al recuperar los datos.</td>
          </tr>
      `;
  }
}

function loadUsuario(idUsuario){
  let request = sendRequest('usuario/list/'+idUsuario, 'GET', '')
  let name = document.getElementById('user-name')
  let password = document.getElementById('user-password')
  let id = document.getElementById('user-id')
  request.onload = function(){
      
      let data = request.response
      id.value = data.idUsuario
      name.value = data.nombreUsuario
      password.value = data.contrasenaUsuario
  }
  request.onerror = function(){
      alert("Error al recuperar los datos.");
  }
}

function deleteUsuario(idUsuario){
  let request = sendRequest('usuario/'+idUsuario, 'DELETE', '')
  request.onload = function(){
      loadData()
  }
}

function saveUsuario(){
  let name = document.getElementById('user-name').value
  let password = document.getElementById('user-password').value
  let id = document.getElementById('user-id').value
  let data = {'idUsuario': id,'nombreUsuario':name,'contasenaUsuario': password }
  let request = sendRequest('usuario/', id ? 'PUT' : 'POST', data)
  request.onload = function(){
      window.location = 'usuario.html';
  }
  request.onerror = function(){
      alert('Error al guardar los cambios.')
  }
}