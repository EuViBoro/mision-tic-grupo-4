/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.O2Grupo4.TiendaVirtual.Dao;

import com.O2Grupo4.TiendaVirtual.Model.Transaccion;
import org.springframework.data.repository.CrudRepository; 

/**
 *
 * @author Eruin Velasquez
 */
public interface TransaccionDao extends CrudRepository<Transaccion, Integer> {
    
}
