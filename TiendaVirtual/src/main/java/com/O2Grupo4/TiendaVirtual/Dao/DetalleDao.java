/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.O2Grupo4.TiendaVirtual.Dao;

import com.O2Grupo4.TiendaVirtual.Model.Detalle;
import org.springframework.data.repository.CrudRepository; 

/**
 *
 * @author Eruin Velasquez
 */
public interface DetalleDao extends CrudRepository<Detalle, Integer> {
    
}
