/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.O2Grupo4.TiendaVirtual.Services;

import com.O2Grupo4.TiendaVirtual.Model.Producto;
import java.util.List;

/**
 *
 * @author Eruin Velasquez
 */
public interface ProductoService {
    
        public Producto save(Producto producto);
        public void delete(Integer id);
        public Producto findById(Integer id);
        public List<Producto> findAll();
    
}
