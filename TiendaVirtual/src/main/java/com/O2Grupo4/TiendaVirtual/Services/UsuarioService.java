/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.O2Grupo4.TiendaVirtual.Services;

import com.O2Grupo4.TiendaVirtual.Model.Usuario;
import java.util.List;

/**
 *
 * @author Eruin Velasquez
 */
public interface UsuarioService {
    
        public Usuario save(Usuario usuario);
        public void delete(Integer id);
        public Usuario findById(Integer id);
        public List<Usuario> findAll(); 
    
    
}
