/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.O2Grupo4.TiendaVirtual.Services;

import com.O2Grupo4.TiendaVirtual.Model.Detalle;
import java.util.List;

/**
 *
 * @author Eruin Velasquez
 */
public interface DetalleService {
    
        public Detalle save(Detalle detalle);
        public void delete(Integer id);
        public Detalle findById(Integer id);
        public List<Detalle> findAll(); 
    
}
