/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O2Grupo4.TiendaVirtual.Services.Implements;

import com.O2Grupo4.TiendaVirtual.Dao.TransaccionDao;
import com.O2Grupo4.TiendaVirtual.Model.Transaccion;
import com.O2Grupo4.TiendaVirtual.Services.TransaccionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Eruin Velasquez
 */

@Service
public class TransaccionServiceImpl implements TransaccionService {
    
    @Autowired
    private TransaccionDao transaccionDao;
    

    @Override
    @Transactional(readOnly=false)
    public Transaccion save(Transaccion transaccion) {
        return transaccionDao.save(transaccion);
    }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        transaccionDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Transaccion findById(Integer id) {
        return transaccionDao.findById(id).orElse(null);
    }

    @Override
    public List<Transaccion> findAll() {
        return (List<Transaccion>) transaccionDao.findAll();
    }
    
    
    
}
