/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.O2Grupo4.TiendaVirtual.Services;

import com.O2Grupo4.TiendaVirtual.Model.Transaccion;
import java.util.List;


/**
 *
 * @author Eruin Velasquez
 */
public interface TransaccionService {
    
        public Transaccion save(Transaccion transaccion);
        public void delete(Integer id);
        public Transaccion findById(Integer id);
        public List<Transaccion> findAll(); 
    
}
