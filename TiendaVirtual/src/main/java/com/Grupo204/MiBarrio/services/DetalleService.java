/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.Grupo204.MiBarrio.services;

import com.Grupo204.MiBarrio.model.Detalle;
import java.util.List;

/**
 *
 * @author Sley
 */
public interface DetalleService {
    
    public Detalle save(Detalle detalle);

    public void delete(Integer id);

    public Detalle findById(Integer id);

    public List<Detalle> findAll();
}
