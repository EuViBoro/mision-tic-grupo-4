/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.Grupo204.MiBarrio.services;

import com.Grupo204.MiBarrio.model.Transaccion;
import java.util.List;

/**
 *
 * @author Sley
 */
public interface TransaccionService {

    public Transaccion save(Transaccion transaccion);
    public void delete(Integer id);
    public Transaccion findById(Integer Id);
    public List<Transaccion> findAll();

}
