/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.Grupo204.MiBarrio.services;

import com.Grupo204.MiBarrio.model.Usuario;
import java.util.List;

/**
 *
 * @author Sley
 */
public interface UsuarioService {
    
    public Usuario save (Usuario usuario);
    public void delete (Integer id);
    public Usuario findById (Integer id);

    public List<Usuario> findAll();
    
}
