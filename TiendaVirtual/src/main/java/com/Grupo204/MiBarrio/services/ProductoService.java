/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.Grupo204.MiBarrio.services;

import com.Grupo204.MiBarrio.model.Producto;
import java.util.List;

/**
 *
 * @author Sley
 */
public interface ProductoService {
    
    public Producto save(Producto producto);
    public void delete(Integer id);
    public Producto findById (Integer id);
    public List<Producto> findAll();
    
}
