/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Grupo204.MiBarrio.services.implement;

import com.Grupo204.MiBarrio.dao.DetalleDao;
import com.Grupo204.MiBarrio.model.Detalle;
import com.Grupo204.MiBarrio.services.DetalleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sley
 */

@Service
public class DetalleServiceImpl implements DetalleService {
    @Autowired
    private DetalleDao detalleDao;

    @Override
    @Transactional(readOnly = false)
    public Detalle save(Detalle detalle) {
        return detalleDao.save(detalle);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        detalleDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Detalle findById(Integer id) {
        return detalleDao.findById(id).orElse(null);
    }

    @Override
    public List<Detalle> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
