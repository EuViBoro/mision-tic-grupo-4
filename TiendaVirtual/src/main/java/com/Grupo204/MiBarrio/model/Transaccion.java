/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Grupo204.MiBarrio.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Sley
 */
@Entity
@Table(name = "transaccion")
public class Transaccion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idtransaccion")
    private Integer idTransaccion;

    @Column(name = "tipotransaccion")
    private String tipoTransaccion;

    @Column(name = "fechatransaccion")
    private String fechaTransaccion;

    @Column(name = "vendedor")
    private String vendedor;

    @Column(name = "comprador")
    private String comprador;

    @Column(name = "total")
    private double total;

    public Integer getIdTransaccion() {
        return idTransaccion;
    }

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    public String getVendedor() {
        return vendedor;
    }

    public String getComprador() {
        return comprador;
    }

    public double getTotal() {
        return total;
    }

    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public void setFechaTransaccion(String fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public void setTotal(double total) {
        this.total = total;
    }

}
