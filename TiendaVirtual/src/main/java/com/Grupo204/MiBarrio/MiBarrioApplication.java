package com.Grupo204.MiBarrio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiBarrioApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiBarrioApplication.class, args);
	}

}
