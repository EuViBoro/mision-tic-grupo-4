/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.Grupo204.MiBarrio.dao;

import com.Grupo204.MiBarrio.model.Detalle;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Sley
 */
public interface DetalleDao extends CrudRepository<Detalle, Integer> {

}
