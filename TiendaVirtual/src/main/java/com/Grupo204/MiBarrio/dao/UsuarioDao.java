/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.Grupo204.MiBarrio.dao;

import com.Grupo204.MiBarrio.model.Usuario;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Sley
 */
public interface UsuarioDao extends CrudRepository<Usuario, Integer> {

}
